1. ¿Qué es *git*?

**Git** es un sistema de control de versiones, usado principalmente para proyectos de software. Es una herramienta que permite ordenar el trabajo en equipo, avanzar de manera ordenada en un proyecto, volver a una versión anterior del proyecto si los cambios realizados no dan buenos resultados.
(slide que muestre: trabajo en equipo, movimiento de un proyecto, sincronización de repositorios... Mostrar como ejemplo lo que ocurre con un proyecto de "texto" extenso, como una tesis (tesis1, tesis2, tesis_final1, tesis_final_final)

