---?image=https://miro.medium.com/max/910/1*JZ2YCpyIOO3JfnXy264b_A.png&position=50% 5%&size=20%&color=#ff9933

<br/><br/>
## @color[black](Guía básica)
<br/><br/>

#### Pablo Cremades (pcremades)

![width=425, height=150 ](https://gitlab.com/pcremades/taller-git/-/raw/master/img/ICB.png)

@snap[south-west span-100 text-04]
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
@snapend

#HSLIDE

## *Git es un sistema de control de versiones distribuido.*

#HSLIDE

@snap[midpoint span-100]
## ¿Para qué sirve?

![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/gitVentajas.png)
@snapend

Note:

- Registra cambios en un conjunto de archivos
- Recuperar versiones anteriores de los archivos
- Comparar cambios respecto a cualquier versión anterior
- Ordenar el trabajo entre varios desarrolladores
- Sincronizar proyectos entre distintos dispositivos

#HSLIDE

@snap[center span-100]

### Descargar para Linux

@box[bg-gray text-white rounded box-padding]($ apt install git)

<br/><br/>

### Descargar para Windows
[Link @fa[external-link]](https://gitforwindows.org/)

@snapend

Note:

- También está para MAC
- Voy a hablar sólo del uso por línea de comando
- Hay GUIs

** Ejercicio: instalar git**

#HSLIDE

## Configuración básica de *git*

@box[bg-gray text-white rounded box-padding])( $ git config --global user.name *nombre*)
<br/><br/>
@box[bg-gray text-white rounded box-padding])( $ git config --global user.email *nombre@servicio*)

Note:

 - Es el usuario con el que quedan registrado los cambios en el repo.
 - Es conveniente usar el mismo usuario que se va a usar en GitLab

#HSLIDE

## Iniciar un repositorio

@box[bg-gray text-white rounded box-padding])( $ git init )

Note:

- Es pedirle a git que empiece a seguir un directorio
- Esto crea una carpeta oculta ".git"

**Ejercicio: iniciar un repo**


#HSLIDE

## Flujo de trabajo

![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/git_workflow.png)


---?image=https://gitlab.com/pcremades/taller-git/-/raw/master/img/trees.png&position=50% 80%&size=50%&color=#ff9933
@snap[north span-100 text-07]
<br/><br/>
El repositorio local esta compuesto por tres "árboles" administrados por git. El primero es el **Directorio** de trabajo que contiene los archivos, 
el segundo es el **Index** que actúa como una zona intermedia, 
y el último es el **HEAD** que apunta al último commit realizado.
@snapend

#HSLIDE

@snap[north span-100]
## add & commit
@snapend

@snap[west span-40 text-07 text-center]
@box[bg-gray text-white rounded box-padding])( $ git add <*nombre de archivo*> )
o
@box[bg-gray text-white rounded box-padding])( $ git add . )
<br/><br/>
@box[bg-gray text-white rounded box-padding])( $ git commit -m "*mensaje*" )
@snapend

@snap[east span-40 text-07 text-center]
ver estado del repositorio
@box[bg-gray text-white rounded box-padding])( $ git status )
<br/><br/>
ver registro de commits
@box[bg-gray text-white rounded box-padding])( $ git log )
[Más información @fa[external-link]](https://git-scm.com/book/en/v2/Git-Basics-Viewing-the-Commit-History)
@snapend


Note:
- ** Ejercicio: crear un archivo y hacer un commit **
- ** Comando status y log **
- ** Agregar contenido al archivo y hacer otro commit **
- git log opciones (-p --stat --graph)
- git log --oneline --decorate --graph --all

#VSLIDE

@snap[midpoint span-100]
## Ignorar archivos
Colocar la lista de archivos que queremos ignorar en:
@box[bg-gray text-white rounded box-padding])( *.gitignore* )
@snapend

#HSLIDE

@snap[north span-100]
## Tags
@quote[Son rótulos que se ponen a los commits]
@snapend

@snap[west span-40 text-07 text-center]
@box[bg-gray text-white rounded box-padding])( $ git tag )
<br/><br/>
@box[bg-gray text-white rounded box-padding])( $ git tag -a *tag* -m "mensaje")
@snapend

@snap[east span-40 text-07 text-center]
@box[bg-gray text-white rounded box-padding])( $ git tag -a *tag* *hash*)
<br/><br/>
@box[bg-gray text-white rounded box-padding])( $ git checkout -b *nombre* tag )
@snapend

@snap[south span-100 text-07]
[Más información @fa[external-link]](https://git-scm.com/book/en/v2/Git-Basics-Tagging)
@snapend

Note:
- los tags no se suben al repo remoto. git push origin --tags
- git show *tag*


---?color=black

@snap[north span-100]
## ramas (branches)
@snapend

@snap[west span-50 text-07 text-center]
@box[bg-gray text-white rounded box-padding])( $ git branch)
<br/><br/>
@box[bg-gray text-white rounded box-padding])( $ git branch *nueva rama*)
<br/><br/>
@box[bg-gray text-white rounded box-padding])( $ git checkout *rama*)
@snapend

@snap[south span-50 text-07 text-center]
[Más información @fa[external-link]](https://git-scm.com/book/en/v2/Git-Branching-Branches-in-a-Nutshell)
@snapend

@snap[east span-50]
![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/branches.png)
@snapend

Note:

 - las ramas son líneas de trabajo paralelas
 - flujo de trabajo: master (copia que funciona), nueva rama para trabajar

** Ejercicio: crear una rama y desarrollar **
** Crear un nuevo archivo (función) y poner llamada en archivo1.txt **

#HSLIDE

## Comparar ramas
@box[bg-gray text-white rounded box-padding])( $ git diff *rama/hash*)
<br/><br/>
@box[bg-gray text-white rounded box-padding])( $ git difftool *rama/hash*)
<br/><br/>
@box[bg-gray text-white rounded box-padding])( $ git difftool --dir-diff *rama/hash*)
 
#VSLIDE

## Configurar difftool
@box[bg-gray text-white rounded box-padding])($ git config diff.tool [meld opendiff kompare vimdiff] )

#HSLIDE

@snap[midpoint span-100 text-07 text-center]
## Fusionar ramas
Desde cualquier rama existente:
@box[bg-gray text-white rounded box-padding])( $ git merge *rama*)
<br/><br/>
### Hay dos posibilidades...
@snapend

#VSLIDE

@snap[west span-50 text-07 text-center]
### Fast-foward
![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/fast_forward.png =550×450)
@snapend
@snap[east span-50 text-07 text-center]
### 3-way merge
![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/3-way.png =550×450)
@snapend

Note:

** Ejercicio1: fusionar rama, fast-forward **

** Ejercicio2: cambios en func y master e intentar merge **

** Hacer git status, comentar sobre git merge --abort **

- Mostrar git log --oneline --decorate --graph --all

#VSLIDE

## Conflictos
![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/mergeconflict.png =700)
@box[bg-gray text-white rounded box-padding])( $ git mergetool )

Note:
- Borrar archivos backup .orig: git config --global mergetool.keepBackup false


#HSLIDE

@snap[midpoint span-100 text-07 text-center]
## Volver a una versión anterior
@box[bg-gray text-white rounded box-padding])( $ git checkout -b xxxx *hash* )

Esto crea una rama nueva llamada *xxxx* contra la que después podemos comparar y fusionar.
<br/><br/>

Alternativamente, podemos revertir definitivamente a un estado anterior:
@box[bg-gray text-white rounded box-padding])( $ git reset --hard *hash* )
@snapend

#HSLIDE

## Recursos

- [Libro oficial](https://git-scm.com/doc)

- [Guía básica](https://rogerdudler.github.io/git-guide/index.es.html)

- [Tutorial de branching](https://learngitbranching.js.org/)


#HSLIDE
---?image=https://blog.desdelinux.net/wp-content/uploads/2019/09/GitLab.jpg&position=50% 5%&size=30%

<br/><br/>
<br/><br/>
## @color[black](Guía básica)
<br/><br/>

#### Pablo Cremades (pcremades)

![width=425, height=150 ](https://gitlab.com/pcremades/taller-git/-/raw/master/img/ICB.png)


#HSLIDE

@snap[midpoint span-100 text-06 text-center]
## GitLab es una plataforma web para administrar repositorios *git*. Además permite crear wikis, hacer seguimiento de issues, y automatizar todo el ciclo de desarrollo y "despliegue"
Permite sincronizar todos los repositorios...
@snapend

#HSLIDE

## Clonar un repositorio
@box[bg-gray text-white rounded box-padding])($ git clone *url* )

Esto crea una carpeta con el nombre del repositorio en el directorio actual.

Note:

- A partir de ahí el trabajo es igual que antes.

#HSLIDE

## Flujo de trabajo

![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/git_workflow.png)

Note:

- Aparecen 2 nuevos árboles en la estructura de trabajo: el repositorio remoto y la referencia local

#VSLIDE

@snap[center span-100 text-06 text-center]
## Clonar un repositorio con múltiples *branches*
Listar todas las ramas del repositorio:
@box[bg-gray text-white rounded box-padding])($ git branch -a )

Checkout una rama:
@box[bg-gray text-white rounded box-padding])($ git checkout *rama* )

@quote[No colocar origin/rama porque eso crea un enlace a la rama del remoto sólo para visualizar el contenido, pero no para trabajar.]
@snapend

Note:

- Cómo traer las ramas del remote...

#VSLIDE

@snap[center span-100 text-06 text-center]
## Subir nuevas ramas locales al servidor remoto

@box[bg-gray text-white rounded box-padding])($ git push -u origin *rama* )
@snapend

#HSLIDE

@snap[midpoint span-100 text-07 text-center]
## Subir cambios al servidor

Subir todos los últimos commits de la **rama actual** al servidor

@box[bg-gray text-white rounded box-padding])($ git push )

Subir todos los últimos commits de la **rama** al servidor (origin)

@box[bg-gray text-white rounded box-padding])($ git push -u origin *rama* )

Muestra la URL del repositorio remoto

@box[bg-gray text-white rounded box-padding])($ git remote show origin )

@snapend

#HSLIDE

## Actualizar repositorio local

La forma directa...
@box[bg-gray text-white rounded box-padding])($ git pull )

#VSLIDE

@snap[midpoint span-100 text-07 text-center]
## Actualizar repositorio local

La forma correcta, *libre de riesgos*...
@box[bg-gray text-white rounded box-padding])($ git fetch )
<br/><br/>
@box[bg-gray text-white rounded box-padding])($ git diff FETCHED_HEAD )
<br/><br/>
@box[bg-gray text-white rounded box-padding])($ git merge )
@snapend

Note:

- Esto evita mergear cambios que no queremos. 
- En este caso son válidas todas las herramientas que vimos antes: difftool y mergetool

#HSLIDE

@snap[midpoint span-100 text-07 text-center]
## Subir un proyecto existente

![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/newProject.png)

Para subir todas las ramas existentes...

@box[bg-gray text-white rounded box-padding])($ git push --all origin )
@snapend

Note:

- GitLab te muestra todas las opciones

#HSLIDE

@snap[north span-100 text-center]
## Documentar
@snapend

@snap[west span-50 text-07 text-center]
<br/><br/>
![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/markdown.png =500x)
[README.md](https://gitlab.com/openflexure/openflexure-microscope)
[Markdown](https://about.gitlab.com/handbook/markdown-guide/)
@snapend
@snap[east span-50 text-07 text-center]
![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/wiki.png =500x)
[Wiki](https://tecnologias.libres.cc/residenciacta/suporte-residencia/wikis/home)
@snapend

Note:
- README - Markdown
- Wiki

#HSLIDE

@snap[north span-100 text-07 text-center]
## Sumar colaboradores
@snapend

@snap[west span-50 text-07 text-center]
![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/members.gif)
@snapend

@snap[east span-50 text-07 text-center]
@quote[Los *developers* no pueden hacer *push* a la rama **master**. En general no pueden modificar una rama "protegida".]

[Premisos](https://docs.gitlab.com/ee/user/permissions.html)
@snapend

Note:

- Por eso es importante el **merge request**.

#HSLIDE

## Merge request

![](https://gitlab.com/pcremades/taller-git/-/raw/master/img/mergeRequest.png)

Note:

- No somos administradores
- Equipo grande de personas
- More friendly

#HSLIDE

@snap[midpoint span-50 text-center]
## ISSUES

@quote[Es la forma de comunicarse con el resto de los desarrolladores]
[Issues](https://docs.gitlab.com/ee/user/project/issues/)
@snapend

Note:

- **Crear un issue, pegar el hash de un commit para referenciarlo, mencionar a una persona.**
- **Hacer un commit mencionando un issue**

#HSLIDE

## Fork

@quote[Es una forma de copiar un proyecto para hacer cambios sin modificar el original. Después uno puede decidir compartir o no dichos cambios]

Es similar a clonar pero a nivel GitLab...

#HSLIDE

## Documentación

[GitLab Docs](https://docs.gitlab.com/ee/README.html)

[GitLab Basics](https://docs.gitlab.com/ee/README.html)

[Video Workflow en GitLab](https://www.youtube.com/watch?v=enMumwvLAug&t=2399s)
